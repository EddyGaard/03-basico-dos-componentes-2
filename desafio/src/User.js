import React from 'react'

const User = props => {
    return <div className={`user-${props.name}`}>{props.children}</div>
}

export default User